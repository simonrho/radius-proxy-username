### Radius proxy

[![N|Solid](https://www.juniper.net/assets/img/logos/juniper/juniper-networks-white-s.png)](https://www.juniper.net/us/en/)

#### What?
This script is a radius proxy converting radius attributes including mac address valu in new mac format xx:xx:xx:xx:xx:xx.
The script uses the radius Juniper VSA Dhcp-Options(26-4874-55) to recognize the DHCP user session from PPPoE session.

#### Affected attributes
| Radius Attributes              | DHCP-session      | PPPoE-session              |
|--------------------------------|-------------------|----------------------------|
| User-Name(1)                   | xx:xx:xx:xx:xx:xx | Do not touch - just bypass |
| Dhcp-Mac-Address(26-4874-56)   | xx:xx:xx:xx:xx:xx | xx:xx:xx:xx:xx:xx          |
| Pppoe-Description(26-4874-24)  | xx:xx:xx:xx:xx:xx | xx:xx:xx:xx:xx:xx          |


#### radius message support matrix
| code                             | supported? |
|----------------------------------|------------|
| access-request                   | yes        |
| accounting-request start         | yes        |
| accounting-interim start         | yes        |
| coa request                      | no         |
| disconnect request               | no         |


![Radius_proxy_role](radius_proxy_role.jpg)

#### Input arguments format
The radius proxy application supports two options.
1. -t : timeout value for customer's radius server response. Default value is 3 seconds.
2. -m : radius proxy mapping entries. multiple entries can be specified with comma separator (',')
>   \<local ip>:\<local port>-<remote ip>:\<remote port>-\<radius secret>, ...
   
  The number of mapping entry is unlimited.
  You can copy jrp.py and rename it and use it if you want to have application redundancy.
  For instance, 
  > jrp1.py for 1.1.1.1:11812-10.0.0.1:1812-juniper<br/>
  > jrp2.py for 1.1.1.1:11813-10.0.0.1:1813-juniper<br/>
  > jrp3.py for 1.1.1.1:21812-10.0.0.1:1812-juniper<br/>
  > jrp4.py for 1.1.1.1:21813-10.0.0.1:1813-juniper
  
  The radius secret key can be a <strong>plain string</strong> or <strong>JUNOS encrypted format</strong> value (starting with $9$ prefix)
  You can copy JUNOS encrypted secret key from radius-server configuration on Junos configuration mode CLI
 
#### installation steps
1. copy jrp.py into MX BNG system.
2. register jrp.py file into JET application with radius proxy mapping arguments.
3. add/update JUNOS radius client configuration pointing new radius proxy server listen ip and port. 

```sh
$ scp jrp.py user@bng1:
$ ssh user@bng1
Last login: Mon Mar 22 03:41:04 2021 from 10.107.36.101
--- JUNOS 20.2R1.10 Kernel 64-bit  JNPR-11.0-20200608.0016468_buil
user@bng1> 
user@bng1> request system scripts refresh-from extension-service file jrp.py url /var/home/user/jrp.py 
refreshing 'jrp.py' from '/var/home/user/jrp.py'
user@bng1> 
user@bng1> file list /var/db/scripts/jet/jrp.py detail 
-rw-r-----  1 root  wheel       9283 Mar 22  03:41 /var/db/scripts/jet/jrp.py
total files: 1

user@bng1>
user@bng1> edit 
Entering configuration mode
The configuration has been changed but not committed

[edit]
user@bng1# show system 
scripts {
    language python3;
}
extensions {
    providers {
        jnpr {
            license-type juniper deployment-scope commercial;
        }
    }
    extension-service {
        application {
            file jrp.py {
                arguments "-m 1.1.1.1:1812-10.0.0.1:1812-$9$JfUi.QF/0BEP5BEcyW8ZUj,1.1.1.1:1813-10.0.0.1:1813-$9$JfUi.QF/0BEP5BEcyW8ZUj";
                daemonize;
                respawn-on-normal-exit;
                username root;
            }
        }
    }
}

[edit]
user@bng1# show interfaces lo0 
unit 0 {
    family inet {
        address 1.1.1.1/32;
    }
}

[edit]
user@bng1# show access radius-server 
1.1.1.1 {
    port 1812;
    accounting-port 1813;
    secret "$9$JfUi.QF/0BEP5BEcyW8ZUj"; ## SECRET-DATA
    timeout 1;
    retry 3;
    max-outstanding-requests 500;
    source-address 1.1.1.1;
}

[edit]
user@bng1# show access profile freeradius 
authentication-order radius;
radius {
    authentication-server 1.1.1.1;
    accounting-server 1.1.1.1;
}
accounting {
    order radius;
}

[edit]

```


#### radius message support matrix
| code                             | supported? |
|----------------------------------|------------|
| access-request                   | yes        |
| accounting-request start         | yes        |
| accounting-interim start         | yes        |
| coa request                      | no         |
| disconnect request               | no         |


#### freeradius server accounting data
```sh
Tue Mar 23 13:51:45 2021
	User-Name = "juniper"
	Acct-Status-Type = Start
	Acct-Session-Id = "61"
	Event-Timestamp = "Mar 23 2021 13:50:39 EDT"
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Framed-Protocol = PPP
	Attr-26.4874.177 = 0x506f72742073706565643a20313030303030306b
	Acct-Authentic = RADIUS
	ERX-Dhcp-Mac-Addr = "ac:16:2d:75:9d:0a"
	Framed-IP-Address = 203.0.0.10
	Framed-IP-Netmask = 255.255.255.255
	NAS-Identifier = "petrel1"
	NAS-Port = 805306568
	NAS-Port-Id = "ge-3/1/0.demux0.3221225522:100-200"
	NAS-Port-Type = Ethernet
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "ac:16:2d:75:9d:0a"
	Attr-26.4874.210 = 0x00000004
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "c6972b3b7c5abe21400063b1fc3680e6"
	Timestamp = 1616521905

Tue Mar 23 13:53:03 2021
	User-Name = "juniper"
	Acct-Status-Type = Stop
	Acct-Session-Id = "61"
	Event-Timestamp = "Mar 23 2021 13:51:56 EDT"
	Acct-Input-Octets = 190
	Acct-Output-Octets = 30
	Acct-Session-Time = 77
	Acct-Input-Packets = 5
	Acct-Output-Packets = 6
	Acct-Terminate-Cause = Lost-Carrier
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Framed-Protocol = PPP
	Acct-Authentic = RADIUS
	ERX-Dhcp-Mac-Addr = "ac:16:2d:75:9d:0a"
	Framed-IP-Address = 203.0.0.10
	Framed-IP-Netmask = 255.255.255.255
	ERX-Input-Gigapkts = 0
	Acct-Input-Gigawords = 0
	NAS-Identifier = "petrel1"
	NAS-Port = 805306568
	NAS-Port-Id = "ge-3/1/0.demux0.3221225522:100-200"
	NAS-Port-Type = Ethernet
	ERX-Output-Gigapkts = 0
	Acct-Output-Gigawords = 0
	ERX-IPv6-Acct-Input-Octets = 0
	ERX-IPv6-Acct-Output-Octets = 0
	ERX-IPv6-Acct-Input-Packets = 0
	ERX-IPv6-Acct-Output-Packets = 0
	ERX-IPv6-Acct-Input-Gigawords = 0
	ERX-IPv6-Acct-Output-Gigawords = 0
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "ac:16:2d:75:9d:0a"
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "c6972b3b7c5abe21400063b1fc3680e6"
	Timestamp = 1616521983

Tue Mar 23 13:53:51 2021
	User-Name = "ac:16:2d:75:9d:0a"
	Acct-Status-Type = Start
	Acct-Session-Id = "63"
	Event-Timestamp = "Mar 23 2021 13:52:44 EDT"
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Acct-Authentic = RADIUS
	ERX-Dhcp-Options = 0x3501013204cb000005370d011c02790f060c28292a1a7703
	ERX-Dhcp-Mac-Addr = "ac:16:2d:75:9d:0a"
	Framed-IP-Address = 203.0.0.5
	Framed-IP-Netmask = 255.255.255.0
	NAS-Identifier = "petrel1"
	NAS-Port = 805306568
	NAS-Port-Id = "ge-3/1/0.demux0.3221225524:100-200"
	NAS-Port-Type = Ethernet
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "ac:16:2d:75:9d:0a"
	Attr-26.4874.189 = 0xcb000001
	Attr-26.4874.210 = 0x00000004
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "35a759cd80c000b23127a7f59ef0716f"
	Timestamp = 1616522031

Tue Mar 23 13:54:38 2021
	User-Name = "ac:16:2d:75:9d:0a"
	Acct-Status-Type = Stop
	Acct-Session-Id = "63"
	Event-Timestamp = "Mar 23 2021 13:53:31 EDT"
	Acct-Input-Octets = 1176
	Acct-Output-Octets = 927
	Acct-Session-Time = 47
	Acct-Input-Packets = 18
	Acct-Output-Packets = 16
	Acct-Terminate-Cause = User-Request
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Acct-Authentic = RADIUS
	ERX-Dhcp-Options = 0x3501013204cb000005370d011c02790f060c28292a1a7703
	ERX-Dhcp-Mac-Addr = "ac:16:2d:75:9d:0a"
	Framed-IP-Address = 203.0.0.5
	Framed-IP-Netmask = 255.255.255.0
	ERX-Input-Gigapkts = 0
	Acct-Input-Gigawords = 0
	NAS-Identifier = "petrel1"
	NAS-Port = 805306568
	NAS-Port-Id = "ge-3/1/0.demux0.3221225524:100-200"
	NAS-Port-Type = Ethernet
	ERX-Output-Gigapkts = 0
	Acct-Output-Gigawords = 0
	ERX-IPv6-Acct-Input-Octets = 0
	ERX-IPv6-Acct-Output-Octets = 0
	ERX-IPv6-Acct-Input-Packets = 0
	ERX-IPv6-Acct-Output-Packets = 0
	ERX-IPv6-Acct-Input-Gigawords = 0
	ERX-IPv6-Acct-Output-Gigawords = 0
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "ac:16:2d:75:9d:0a"
	Attr-26.4874.189 = 0xcb000001
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "35a759cd80c000b23127a7f59ef0716f"
	Timestamp = 1616522078



```
#### Radius proxy log messages
```shell
tail -f /var/log/jrp.log
2021-03-23 17:50:22.137 INFO 369: **********************
2021-03-23 17:50:22.137 INFO 370: radius proxy starts!!!
2021-03-23 17:50:22.138 INFO 371: **********************
2021-03-23 17:50:22.138 DEBUG 372: arguments: ['/var/run/scripts/jet//jrp.py', '-m', '1.1.1.1:1812-10.0.0.1:1812-$9$JfUi.QF/0BEP5BEcyW8ZUj,1.1.1.1:1813-10.0.0.1:1813-$9$JfUi.QF/0BEP5BEcyW8ZUj']
2021-03-23 17:50:22.141 DEBUG 390: proxy mapping: 1.1.1.1:1812 <==> 10.0.0.1:1812 : $9$JfUi.QF/0BEP5BEcyW8ZUj
2021-03-23 17:50:22.143 DEBUG 390: proxy mapping: 1.1.1.1:1813 <==> 10.0.0.1:1813 : $9$JfUi.QF/0BEP5BEcyW8ZUj
2021-03-23 17:50:38.367 DEBUG 313: access-request id: 211 pppoe-session: "ac16.2d75.9d0a" -> "ac:16:2d:75:9d:0a"
2021-03-23 17:50:38.667 DEBUG 313: accounting-request id: 212 pppoe-session: "ac16.2d75.9d0a" -> "ac:16:2d:75:9d:0a"
2021-03-23 17:51:56.167 DEBUG 313: accounting-request id: 213 pppoe-session: "ac16.2d75.9d0a" -> "ac:16:2d:75:9d:0a"
2021-03-23 17:52:43.826 DEBUG 309: access-request id: 214 dhcp-session: "ac16.2d75.9d0a" -> "ac:16:2d:75:9d:0a"
2021-03-23 17:52:44.130 DEBUG 309: accounting-request id: 215 dhcp-session: "ac16.2d75.9d0a" -> "ac:16:2d:75:9d:0a"
2021-03-23 17:52:52.167 DEBUG 313: access-request id: 216 pppoe-session: "ac16.2d75.9d0a" -> "ac:16:2d:75:9d:0a"
2021-03-23 17:52:52.367 DEBUG 313: accounting-request id: 217 pppoe-session: "ac16.2d75.9d0a" -> "ac:16:2d:75:9d:0a"
2021-03-23 17:53:19.567 DEBUG 313: accounting-request id: 218 pppoe-session: "ac16.2d75.9d0a" -> "ac:16:2d:75:9d:0a"
2021-03-23 17:53:31.116 DEBUG 309: accounting-request id: 219 dhcp-session: "ac16.2d75.9d0a" -> "ac:16:2d:75:9d:0a"

```

#### software version
##### JUNOS
```sh
user@bng1# run show version 
Hostname: bng1
Model: mx480
Junos: 20.2R1.10
JUNOS OS Kernel 64-bit  [20200608.0016468_builder_stable_11]
JUNOS OS libs [20200608.0016468_builder_stable_11]
JUNOS OS runtime [20200608.0016468_builder_stable_11]
JUNOS OS time zone information [20200608.0016468_builder_stable_11]
JUNOS network stack and utilities [20200625.123713_builder_junos_202_r1]
JUNOS libs [20200625.123713_builder_junos_202_r1]
JUNOS OS libs compat32 [20200608.0016468_builder_stable_11]
JUNOS OS 32-bit compatibility [20200608.0016468_builder_stable_11]
JUNOS libs compat32 [20200625.123713_builder_junos_202_r1]
JUNOS runtime [20200625.123713_builder_junos_202_r1]
JUNOS sflow mx [20200625.123713_builder_junos_202_r1]
JUNOS py extensions2 [20200625.123713_builder_junos_202_r1]
JUNOS py extensions [20200625.123713_builder_junos_202_r1]
JUNOS py base2 [20200625.123713_builder_junos_202_r1]
JUNOS py base [20200625.123713_builder_junos_202_r1]
JUNOS OS crypto [20200608.0016468_builder_stable_11]
JUNOS na telemetry [20.2R1.10]
JUNOS Security Intelligence [20200625.123713_builder_junos_202_r1]
JUNOS mx libs compat32 [20200625.123713_builder_junos_202_r1]
JUNOS mx runtime [20200625.123713_builder_junos_202_r1]
JUNOS RPD Telemetry Application [20.2R1.10]
Redis [20200625.123713_builder_junos_202_r1]
JUNOS common platform support [20200625.123713_builder_junos_202_r1]
JUNOS Openconfig [20.2R1.10]
JUNOS mtx network modules [20200625.123713_builder_junos_202_r1]
JUNOS modules [20200625.123713_builder_junos_202_r1]
JUNOS mx modules [20200625.123713_builder_junos_202_r1]
JUNOS mx libs [20200625.123713_builder_junos_202_r1]
JUNOS SQL Sync Daemon [20200625.123713_builder_junos_202_r1]
JUNOS mtx Data Plane Crypto Support [20200625.123713_builder_junos_202_r1]
JUNOS daemons [20200625.123713_builder_junos_202_r1]
JUNOS mx daemons [20200625.123713_builder_junos_202_r1]
JUNOS appidd-mx application-identification daemon [20200625.123713_builder_junos_202_r1]
JUNOS Services URL Filter package [20200625.123713_builder_junos_202_r1]
JUNOS Services TLB Service PIC package [20200625.123713_builder_junos_202_r1]
JUNOS Services Telemetry [20200625.123713_builder_junos_202_r1]
JUNOS Services TCP-LOG [20200625.123713_builder_junos_202_r1]
JUNOS Services SSL [20200625.123713_builder_junos_202_r1]
JUNOS Services SOFTWIRE [20200625.123713_builder_junos_202_r1]
JUNOS Services Stateful Firewall [20200625.123713_builder_junos_202_r1]
JUNOS Services RTCOM [20200625.123713_builder_junos_202_r1]
JUNOS Services RPM [20200625.123713_builder_junos_202_r1]
JUNOS Services PCEF package [20200625.123713_builder_junos_202_r1]
JUNOS Services NAT [20200625.123713_builder_junos_202_r1]
JUNOS Services Mobile Subscriber Service Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services MobileNext Software package [20200625.123713_builder_junos_202_r1]
JUNOS Services Logging Report Framework package [20200625.123713_builder_junos_202_r1]
JUNOS Services LL-PDF Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services Jflow Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services Deep Packet Inspection package [20200625.123713_builder_junos_202_r1]
JUNOS Services IPSec [20200625.123713_builder_junos_202_r1]
JUNOS Services IDS [20200625.123713_builder_junos_202_r1]
JUNOS IDP Services [20200625.123713_builder_junos_202_r1]
JUNOS Services HTTP Content Management package [20200625.123713_builder_junos_202_r1]
JUNOS Services Crypto [20200625.123713_builder_junos_202_r1]
JUNOS Services Captive Portal and Content Delivery Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services COS [20200625.123713_builder_junos_202_r1]
JUNOS AppId Services [20200625.123713_builder_junos_202_r1]
JUNOS Services Application Level Gateways [20200625.123713_builder_junos_202_r1]
JUNOS Services AACL Container package [20200625.123713_builder_junos_202_r1]
JUNOS SDN Software Suite [20200625.123713_builder_junos_202_r1]
JUNOS Extension Toolkit [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (wrlinux9) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (MXSPC3) [20.2R1.10]
JUNOS Packet Forwarding Engine Support (MX/EX92XX Common) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (M/T Common) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (aft) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (MX Common) [20200625.123713_builder_junos_202_r1]
JUNOS Juniper Malware Removal Tool (JMRT) [1.0.0+20200625.123713_builder_junos_202_r1]
JUNOS J-Insight [20200625.123713_builder_junos_202_r1]
JUNOS jfirmware [20200625.123713_builder_junos_202_r1]
JUNOS Online Documentation [20200625.123713_builder_junos_202_r1]
JUNOS jail runtime [20200608.0016468_builder_stable_11]

```

##### Python
```sh
python3.7
```